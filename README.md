# Features

* Takes requests (with no certainty that they'll be played)
* Announce the title and artist of tracks occasionally.
* Announce the current time at the top of the hour.
* Live display of what's playing in web browser.
* Show the entire library in web browser (for ease of requesting)

## Abortive features

* Keep and allow display of last-played tracks (out of time).
* Stream to web browsers (gstreamer is poorly-documented)
* Analytics to do cleaner transitions (gstreamer is poorly-documented)

# Startup

Start a Festival server on the same machine as Biribiri, on the default
listening port:

    festival --server

Create the database in your cwd:

    python2 biribiri/scripts/createdb.py

Run the library loader to populate the database with tracks. There must be
some tracks in the db before starting the web interface.

    python2 biribiri/scripts/loadlibrary.py /path/to/music

Start the server and playback:

    python2 biribiri/scripts/run.py

Playback should start automatically on the local system. If you have trouble
with gstreamer errors, try setting GST\_DEBUG to a nonzero value in your
environment. Larger values will be more verbose.

    export GST_DEBUG=2

