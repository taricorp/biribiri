from setuptools import setup, find_packages

setup(
    name = "biribiri",
    version = "0",
    packages = find_packages(),
    requires = ['mutagen', 'flask']
)
