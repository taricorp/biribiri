#!/usr/bin/env python2
from __future__ import print_function

import functools, os, sys
import mutagen

from biribiri import db
from biribiri.analytics import fileDuration, fileBPM#, fileSpectrum

import sqlalchemy.exc

if len(sys.argv) < 2:
    print("Missing path to scan.")
    sys.exit(1)

root = sys.argv[1]
print("Scanning directory tree rooted at", root)

db.create_tables()
s = db.Session()

for path, _, files in os.walk(root):
    fillpath = functools.partial(os.path.join, path)
    for file_name in map(fillpath, files):
        # Open file to get meta-info
        meta = mutagen.File(file_name, easy=True)
        if meta is None:
            continue

        print(file_name)
        if s.query(db.Track).filter(db.Track.filename==file_name).count() != 0:
            print('\tSkipped (already in db)')
            continue

        duration = fileDuration(file_name)
        bpm = fileBPM(file_name)
        title = meta['title'][0] if 'title' in meta else None
        artist = meta['artist'][0] if 'artist' in meta else None
        album = meta['album'][0] if 'album' in meta else None
        try:
            tracknumber = int(meta['tracknumber'][0])
        except ValueError, KeyError:
            tracknumber = 0

        try:
            t = db.Track(filename = file_name,
                         duration = duration,
                         title = title,
                         artist = artist,
                         album = album,
                         tracknumber = tracknumber,
                         bpm = bpm)
            s.add(t)
            s.commit()
            try:
                print("\t{0} {1} - '{2}'; {3} #{4} @ {5} bpm"
                      .format(duration, artist, title, album, tracknumber, bpm))
            except UnicodeEncodeError:
                print("\tOK (can't print this track's info due to terminal encoding fun)")
        except sqlalchemy.exc.ProgrammingError:
            print("\tSkipped; exception:", e)
