#!/usr/bin/env python2
import sys

DEBUG = False
for arg in sys.argv[1:]:
    if arg.lower() == 'debug':
        DEBUG = True
        print("Debug mode activated. Hold on to your butts.")

import biribiri.mediaserver
server = biribiri.mediaserver.MediaServer()
server.start()
print("Server started")

import biribiri.web
biribiri.web.app.media_player = server
biribiri.web.app.run('0.0.0.0', 8000, debug=DEBUG)
