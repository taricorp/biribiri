from __future__ import print_function
from biribiri import musicplayer
import socket
import threading

class MediaServer(threading.Thread):
  def __init__(self):
    super(MediaServer, self).__init__()
    self.daemon = True

    self.mediaPlayer = musicplayer.MusicPlayer()
    self.mediaPlayer.start()
    
  def serve(self):
    host = '0.0.0.0'
    port = 8001
        
    return
    # Disabled
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(1)

    while True:
        conn, addr = s.accept()
        self.mediaPlayer.addStream(conn)

  run = serve
    
  def currentPlayingPosition(self):
    return self.mediaPlayer.currentPlayingPosition()
  def currentPlayingDuration(self):
    return self.mediaPlayer.currentPlayingDuration()
  def currentPlayingArtist(self):
    return self.mediaPlayer.currentPlayingArtist()
  def currentPlayingTitle(self):
    return self.mediaPlayer.currentPlayingTitle()

    
