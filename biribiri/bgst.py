import gobject
gobject.threads_init()
import glib

import pygst
pygst.require('0.10')
import gst
import gst.pbutils

import sys
import os.path

WIN32 = sys.platform == 'win32'

# GstPlayFlags enumeration
GST_PLAY_FLAG_VIDEO         = (1 << 0)
GST_PLAY_FLAG_AUDIO         = (1 << 1)
GST_PLAY_FLAG_TEXT          = (1 << 2)
GST_PLAY_FLAG_VIS           = (1 << 3)
GST_PLAY_FLAG_SOFT_VOLUME   = (1 << 4)
GST_PLAY_FLAG_NATIVE_AUDIO  = (1 << 5)
GST_PLAY_FLAG_NATIVE_VIDEO  = (1 << 6)
GST_PLAY_FLAG_DOWNLOAD      = (1 << 7)
GST_PLAY_FLAG_BUFFERING     = (1 << 8)
GST_PLAY_FLAG_DEINTERLACE   = (1 << 9)
GST_PLAY_FLAG_SOFT_COLORBALANCE = (1 << 10)

# GstRecoverPolicy enumeration
GST_RECOVER_POLICY_NONE = 0
GST_RECOVER_POLICY_RESYNC_LATEST = 1
GST_RECOVER_POLICY_RESYNC_SOFT_LIMIT = 2
GST_RECOVER_POLICY_RESYNC_KEYFRAME = 3


class Pipeline(object):
  
  def __init__(self):
    self.mainLoop = glib.MainLoop()
    self.pipeline = gst.element_factory_make('playbin2')

    self.messageHandler = None
    self.pipeline.set_property('flags', GST_PLAY_FLAG_AUDIO)
    self.pipeline.set_property('video-sink', gst.element_factory_make('fakesink'))

  def setMessageHandler(self, handler):
    self.messageHandler = handler
  
  def getBus(self):
    return self.pipeline.get_bus()
    
  def play(self, filename):
    print(filename, os.path.abspath(filename))
    if WIN32:
        # Unix always has a leading slash on that, but windows doesn't
        filename = 'file:///' + os.path.abspath(filename)
    else:
        filename = 'file://' + os.path.abspath(filename)
    self.pipeline.set_property('uri', filename)
    self.pipeline.set_state(gst.STATE_PLAYING)
    bus = self.pipeline.get_bus()
    if self.messageHandler != None:
      while True:
        message = bus.pop()
        if message == None:
          continue
        if message.type == gst.MESSAGE_ERROR:
          break
        elif message.type == Gst.MESSAGE_EOS:
          break
      self.pipeline.set_state(gst.STATE_NULL)
    
  def currentPlayingPos(self):
    return self.pipeline.query_position(gst.FORMAT_TIME)
  def currentPlayingDuration(self):
    return self.pipeline.query_duration(gst.FORMAT_TIME)
