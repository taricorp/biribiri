from __future__ import print_function
import time
import os
import sys
import random
from threading import Thread
from biribiri import bgst
from biribiri import db

from biribiri.db import Session, Track, Request
from sqlalchemy.sql.expression import func


import pygst
pygst.require('0.10')
import gst
import gst.pbutils

import tempfile
import subprocess



#Gst.init(None)

shortMessages = ["You're listening to biribiri radio.",
                  "biribiri!",
                  "Stay tuned for more electrifying music.",
                  "Reduce C O 2 emmisions. Breathe less."]

previousTrackMessages = ["That was ",
                         "You just heard ",
                         "That track was "]
twoBackMessages = [" and before that was ",
                   " which was precceded by "]
nextTrackMessages = [" Up next is ",
                     " Here is "]
requestMessages = ["By request, here is ",
                   "This track goes out by request:",
                   "By request"]
jokes = ["How about this time you tell the joke instead?",
         "How do you make an octopus laugh? Ten tickles!",
         "A man walks into a bar. There are no survivors.",
         "How do you get a Harvard graduate off your porch? Pay him for the pizza.",
         "A marine walks into a bar. There is no counter",
         "An E flat note, a C note, and a G note walk into a bar. The barman says, Sorry, we don't serve minors here. C sends E flat a dirty look. I told you to act natural!",
         "Why did the scarecrow win an award? Because he was outstanding in his field.",
         "Knock knock.",
         "Olympic gymnast walks into a bar, she doesn't get a medal.",
         "You can tune a piano, but you can't tuna fish.",
         "How do you organize a party in space? - You planet!!",
         "What do you call a psychic midget who has escaped from prison? A small medium at large. ",
         "What do you call a fake noodle? An impasta!",
         "A grasshopper walks into a bar and the bartender says, Hey! We have a drink named after you! Confused, the grasshopper replies, You have a drink named Steve?"]

httpResponse = ['HTTP/1.1 200 OK\r\n',
                'Content-Type: audio/ogg\r\n'
                'Connection: Close\r\n'
                '\r\n']


class MusicPlayer(Thread):
  def __init__(self):
    super(MusicPlayer, self).__init__()
    self.daemon = True

    self.queue = []
    self.unannouncedSongs = []
    self.justAnnounced = False
    self.audioPipe = bgst.Pipeline()
    self.nextSongByRequest = False

    self.currentSongTitle = None
    self.currentSongArtist = None

    self.nextTrack = None
    self.selectNextTrack()
  
  def announceIfNeeded(self):
    if time.localtime().tm_min < 3 or time.localtime().tm_min > 57:
      #long Message
      self.say("You're listening to biribiri radio. The current time is " + time.asctime(time.localtime())) 
    if (18 < time.localtime().tm_min < 22 ) or ( 38 < time.localtime().tm_min < 42 ):
      #short message
      message = random.randint(0,4)
      self.say(random.choice(shortMessages))
    if self.nextSongByRequest == True:
      self.announceTracks()
    if len(self.unannouncedSongs) > 1:
      self.announceTracks()
    
  def say(self, Message):
    tf = tempfile.NamedTemporaryFile()
    tf.write(Message)
    tf.flush()
    
    ret = subprocess.call(['gst-launch-0.10', 'filesrc', 'location='+tf.name, '!', 'festival', '!', 'filesink', 'location=/tmp/djoutput'])
    self.audioPipe.play('/tmp/djoutput')
    buss = self.audioPipe.getBus()
    while True:
      msg = buss.pop_filtered(gst.MESSAGE_ERROR | gst.MESSAGE_EOS)
      
      if msg:
        if msg.type == gst.MESSAGE_ERROR:
          err, debug = msg.parse_error()
          print("Error received from element %s: %s" % (
              msg.src.get_name(), err), file=sys.stderr)
          finished = True
          self.audioPipe.pipeline.set_state(gst.STATE_NULL)
          break
        if msg.type == gst.MESSAGE_EOS:
          self.audioPipe.pipeline.set_state(gst.STATE_NULL)
          break
    ret = subprocess.call(['rm', '/tmp/djoutput'])

    print(Message)
  
  def announceTracks(self):
    if(len(self.unannouncedSongs) > 0):
      self.say(random.choice(previousTrackMessages) +
               self.unannouncedSongs[-1].title + ' by ' + self.unannouncedSongs[-1].artist)
    if(len(self.unannouncedSongs) > 1):
      self.say(random.choice(twoBackMessages) +
               self.unannouncedSongs[-2].artist+"'s " + self.unannouncedSongs[-2].title)
    if self.nextSongByRequest == True:
      self.say(random.choice(requestMessages))
    else:
      self.say(random.choice(nextTrackMessages))
    self.say(self.nextTrack.artist + " with " + self.nextTrack.title)
    if random.randint(0,1) == 0:
      self.say(random.choice(jokes))
    self.justAnnounced = True
    self.unannouncedSongs = []
    
  def playNextTrack(self):
    self.nextSongByRequest = False
    if self.justAnnounced == True:
      self.justAnnounced = False
    else:
      self.unannouncedSongs.append(self.nextTrack)
    self.audioPipe.play(self.nextTrack.filename)
    self.currentSongArtist = self.nextTrack.artist
    self.currentSongTitle = self.nextTrack.title
    self.selectNextTrack()
  
  def selectNextTrack(self):
    s = Session()
    request = s.query(Request).order_by(func.random()).first()
    if request:
        self.nextSongByRequest = True
        tracks = request.buildQuery(s)
        s.delete(request)
    else:
        tracks = s.query(Track)
    self.nextTrack = tracks.order_by(func.random()).first()
    s.commit()
    # Request may have become invalid somehow, so ensure we don't hang.
    if self.nextTrack is None:
        return self.selectNextTrack()
    print("Next Track: " + self.nextTrack.title)

  
  def playMusic(self):
    finished = False
    while not finished:
      self.playNextTrack()
      buss = self.audioPipe.getBus()
      while True:
        msg = buss.pop_filtered(gst.MESSAGE_ERROR | gst.MESSAGE_EOS)
        
        if msg:
          if msg.type == gst.MESSAGE_ERROR:
            err, debug = msg.parse_error()
            print("Error received from element %s: %s" % (
                msg.src.get_name(), err), file=sys.stderr)
            finished = True
            self.audioPipe.pipeline.set_state(gst.STATE_NULL)
            break
          if msg.type == gst.MESSAGE_EOS:
            self.audioPipe.pipeline.set_state(gst.STATE_NULL)
            break
      self.audioPipe.pipeline.set_state(gst.STATE_NULL)
      self.announceIfNeeded()

  run = playMusic
      
  def currentPlayingPosition(self):
    return self.audioPipe.currentPlayingPos()[0]
  def currentPlayingDuration(self):
    return self.audioPipe.currentPlayingDuration()[0]
  def currentPlayingArtist(self):
    return self.currentSongArtist
  def currentPlayingTitle(self):
    return self.currentSongTitle

