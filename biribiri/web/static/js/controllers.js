'use strict';

/* Controllers */

angular.module('biribiri.controllers', []).
    controller('HistoryCtrl', [function() {

    }])
    .controller('LibraryCtrl', ['$scope', '$resource', function($scope, $resource) {
        $scope.tracks = [];

        var LibraryTracks = $resource('/library/:start');

        var fetchSuccess = function (data) {
            $scope.tracks = $scope.tracks.concat(data.tracks);
        };
        var fetchFailed = function () {

        };
        var getMoreTracks = function(start) {
            start = start || 0;
            LibraryTracks.get({'start': start}, fetchSuccess, fetchFailed);
        };
        getMoreTracks();
    }])
    .controller('RequestFormCtrl', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {
        $scope.artist = '';
        $scope.album = '';
        $scope.track = '';

        $scope.submitted = false;
        $scope.submit_failed = false;

        $scope.submit_request = function () {
            $http.post('/request', {
                'artist': $scope.artist,
                'album': $scope.album,
                'track': $scope.track
            }).success(function (data) {
                if (data != 'OK') {
                    // Nothing in the library that matched.
                    $scope.error = true;
                    $scope.error_message = "I don't have anything like that!";
                } else {
                    $scope.error = false;
                    $scope.submitted = true;
                    $timeout(function () { $scope.submitted = false; }, 5000);
                }
            }).error(function () {
                $scope.submitted = false;
                $scope.error = true;
                $scope.error_message = "You didn't request anything!";
            });
        }
    }])
    .controller('PlaybackCtrl', ['$scope', '$sce', '$location', function ($scope, $sce, l) {
        var ep = l.protocol() + '://' + l.host() + ':' + (l.port() + 1);
        // "Yes, Angular, we trust this URI. (HACK HACK HACK EWWWWW)"
        var ep = $sce.trustAsResourceUrl(ep);
        $scope.endpoint = ep;
    }])
    .controller('NowPlayingCtrl', ['$scope', '$resource', '$timeout', function($scope, $resource, $timeout) {
        $scope.title = "Unknown";
        $scope.artist = "Unknown";
        $scope.duration = 0;

        $scope.elapsed = 0;
        var updateElapsed = function () {
            $scope.elapsed++;
            // Ask the server what's up now when we hit time
            if ($scope.elapsed >= $scope.duration)
                $timeout(query, 1000)
            else
                $timeout(updateElapsed, 1000);
        }

        $scope.retryTimer = 1;
        $scope.troubles = false;
  
        var NowPlaying = $resource('/nowplaying');
        var fetchSuccess = function(data, status) {
            $scope.troubles = false;
            $scope.retryTimer = 1;

            $scope.title = data.title || "Unknown";
            $scope.artist = data.artist || "Unknown";
            $scope.duration = Math.floor(data.duration || 60);
            $scope.elapsed = Math.floor(data.elapsed || 0);

            $timeout(updateElapsed, 0);
        };
        var fetchFailed = function() {
            $scope.troubles = true;
            if ($scope.retryTimer < 1024)
                $scope.retryTimer <<= 1;
            $timeout(query, $scope.retryTimer * 1000);
        };
  
        var query = function() { NowPlaying.get(fetchSuccess, fetchFailed); }
        $scope.refresh = query;
        query();
    }]);
