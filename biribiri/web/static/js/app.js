'use strict';


// Declare app level module which depends on filters, and services
angular.module('biribiri', [
  'ngRoute',
  'ngResource',
  'biribiri.filters',
  'biribiri.services',
  'biribiri.directives',
  'biribiri.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/history', {templateUrl: 'partials/history.html', controller: 'HistoryCtrl'});
  $routeProvider.when('/library', {templateUrl: 'partials/library.html', controller: 'LibraryCtrl'});
  $routeProvider.otherwise({redirectTo: '/library'});
}]);
