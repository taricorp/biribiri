'use strict';

/* Filters */

angular.module('biribiri.filters', []).
    filter('formattime', [function() {
        return function(time) {
            var seconds = String(time % 60);
            if (seconds.length < 2)
                seconds = '0' + seconds;
            var minutes = String(Math.floor(time / 60));
            return minutes + ':' + seconds;
        }
    }]);
