#!/usr/bin/env python
from biribiri.db import Session, Track, Request

import flask
from flask import Flask, redirect, abort
from flask import json
app = Flask(__name__)

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Track):
            return {
                'id': o.id,
                'title': o.title,
                'artist': o.artist,
                'album': o.album,
                'tracknumber': o.tracknumber,
                'duration': o.duration,
            }
        return super(JSONEncoder, self).default(o)
app.json_encoder = JSONEncoder

@app.route('/')
def index():
    return redirect('/static/index.html', code=307)

@app.route('/nowplaying')
def nowplaying():
    if app.media_player:
        elapsed = app.media_player.currentPlayingPosition()
        if elapsed:
            elapsed = float(elapsed) / 1e9
        duration = app.media_player.currentPlayingDuration()
        if duration:
            duration = float(duration) / 1e9
        title = app.media_player.currentPlayingTitle()
        artist = app.media_player.currentPlayingArtist()
        return flask.jsonify({
            'elapsed': elapsed,
            'duration': duration,
            'title': title,
            'artist': artist
        })
    abort(503)

@app.route('/library')
def libraryRedir():
    return redirect('/library/0')

@app.route('/library/<start>')
def enumerateLibrary(start):
    LIMIT = 100
    s = Session()
    tracks = s.query(Track).filter(Track.id >= start)
    return flask.json.jsonify({
        'more': tracks.count() > LIMIT,
        'tracks': tracks[:100]
    })

@app.route('/request', methods=['POST'])
def takeRequest():
    q = flask.request.get_json()
    artist = q['artist'] or None
    album = q['album'] or None
    title = q['track'] or None
    print(artist, album, title)
    if not (artist or album or title):
        abort(400)
    s = Session()
    req = Request(artist=artist, album=album, track=title)
    if req.buildQuery(s).count() == 0:
        print("Nothing matching request")
        return "No matches";
    else:
        print("New request " + (artist or '') + (album or '') + (title or ''))
        s.add(req)
        s.commit()
        return 'OK'

if __name__ == '__main__':
    app.run(debug=True)
