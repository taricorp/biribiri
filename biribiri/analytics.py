from __future__ import print_function
import sys
from biribiri import bgst

import pygst
pygst.require('0.10')
import gst
import gst.pbutils

import math
 
#Gst.init(None)

def fileBPM(file_name):
  return 0

  bpm = None
  pipeline = bgst.Pipeline()
  bpmElement = gst.element_factory_make('bpmdetect', 'BPM')
  audioconvert = gst.element_factory_make('audioconvert', 'BPM')
  audioconvert.link(bpmElement)
  pipeline.appendSink(bpmElement)
  bus = pipeline.getBus()
  pipeline.play(file_name)
  while True:
    msg = bus.pop_filtered(gst.MESSAGE_INFO | gst.MESSAGE_ERROR | gst.MESSAGE_EOS)
    if msg:
      t = msg.type
      if t == gst.MESSAGE_INFO:
        output = msg.parse_info()
        print("Output: "+ output, file=sys.stderr)
        if "Detected BPM:" in output:
          bpm = int( math.floor(float(output.replace("Detected BPM:",'',1))))
      elif t == gst.MESSAGE_ERROR:
        err, debug = msg.parse_error()
        print('Playback error:', err, debug, file=sys.stderr)
        bpm = 0
        break
      elif t == gst.MESSAGE_EOS:
        if bpm == None:
          bpm = 0
        break
  return bpm

def fileDuration(file_name):
    return 0
