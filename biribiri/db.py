
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship, sessionmaker
from sqlalchemy.sql.expression import func

# Here's a hack for 8-bit bytestrings (workaround for some kind of Python2
# stupidity with unicode).
from sqlalchemy.interfaces import PoolListener
class SetTextFactory(PoolListener):
     def connect(self, dbapi_con, con_record):
         dbapi_con.text_factory = str

engine = create_engine('sqlite:///biribiri.sqlite', listeners=[SetTextFactory()])
Base = declarative_base()
Session = sessionmaker(bind=engine)

from sqlalchemy import Column, Integer, Float, Unicode, String, ForeignKey

class Track(Base):
    """Represents a music file."""
    __tablename__ = 'tracks'

    id = Column(Integer, primary_key=True)
    filename = Column(String(length=128), nullable=False, unique=True)

    duration = Column(Float)
    title = Column(Unicode(length=64))
    artist = Column(Unicode(length=64))
    album = Column(Unicode(length=64))
    tracknumber = Column(Integer)
    bpm = Column(Integer)
    #spectrum = Column()

    def __repr__(self):
        return "<Track('%s')>" %self.filename

class Request(Base):
    __tablename__ = "requests"

    id = Column(Integer, primary_key=True)
    artist = Column(Unicode(length=64))
    album = Column(Unicode(length=64))
    track = Column(Unicode(length=64))

    def buildQuery(self, s):
        matches = s.query(Track)
        (artist, album, track) = (self.artist, self.album, self.track)
        if artist:
            matches = matches.filter(func.upper(Track.artist).like('%' + artist.upper() + '%'))
        if album:
            matches = matches.filter(func.upper(Track.album).like('%' + album.upper() + '%'))
        if track:
            matches = matches.filter(func.upper(Track.title).like('%' + title.upper() + '%'))
        return matches


def create_tables():
    Base.metadata.create_all(engine)

if __name__ == '__main__':
    create_tables()
